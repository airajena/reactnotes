/*

React is a javascript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications. However, React is only concerned with state management and rendering that state to the DOM, so creating React applications usually requires the use of additional libraries for routing, as well as certain client-side functionality.

while libraries provide specific functionalities that can be used in a modular way, frameworks provide a higher-level structure and guide the overall architecture of an application
*/