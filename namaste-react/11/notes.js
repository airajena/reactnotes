/*
React Redux 
Redux Toolkit

^Redux Toolkit^
Restaurant Menu 
Redux Store: 
It is a single source of truth for the entire application. It is a plain big object that contains the entire state of the application.  

Redux Store has different Slices. It is used for Logical Separations of the store.

We have cart component and Add Item button Restaurant Menu. On clicking on Add Item , the respective Item gets added to the Cart.

Steps:
Click on Add Item Button
It dispatches an Action which calls a Reducer function.
This Reducer function updates the slice(cart slice in this case) of the store.
We Subscribe to the store to read the data using Selector.
This Selector is in Sync with the store and updates the cart component.
This is how the data flows in Redux.





*/

/*
Insatll Redux Toolkit and React Redux
Build Redux Store
Connect Redux Store to React App
Create Slices of the Store
Dispatch Actions
Read Data from Store(Selctor)
Update Store using Reducers

& Configure Store - To create the store
import { configureStore } from '@reduxjs/toolkit';
import cartSlice from './cart-slice';

const store = configureStore({
    reducer: {   // reducer of the app store which conatins all the slices
        cart: cartSlice, // cart is the name of the cart slice
    },
});

export default store;

& Provider- To provide the store to the entire application
import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

& createSlice - To create a slice of the store
import { createSlice } from '@reduxjs/toolkit';

export const cartSlice = createSlice({  // cartSlice is the name of the slice 
    name: 'cart',  // name of the slice
    initialState:{  // initial state of the slice
        items:[],
        totalQuantity:0,
    },
    reducers: {       // reducers are the functions that will update the slice
        addItem:(state,action) => {   // addItem is the name of the reducer 
            state.items.push(action.payload);  //reducer funtion 
        },
        removeItem:(state,action) => {   // removeItem is the name of the reducer 
            state.items.pop(action.payload);
        },
    },
});

export const { addItem, removeItem } = cartSlice.actions;  // exporting the actions
export default cartSlice.reducer;  // exporting the slice

&useSelector - To read the updated slice
import { useSelector } from 'react-redux';

const cartItems = useSelector((store) => store.cart.items); // cart is the name of the slice

& useDispatch - To dispatch an action
import { useDispatch } from 'react-redux';
import { addItem } from './store/cart-slice';

const dispatch = useDispatch();

const handleAddToCart = () => {
    dispatch(addItem({id:1, name:'Pizza', price:12.99}));  // dispatching the action(dispatch is a funtion)
}

onClick={handleAddToCart}  // calling the function on click
*/